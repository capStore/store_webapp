import { useSelector, useDispatch } from "react-redux";
import { addProductToCart, deleteProductFromCart } from "../module/store/cart";

export const useCart = () => {
  const { cart, status } = useSelector(state => state.cart);
  const dispatch = useDispatch();

  const addProduct = async (fields, setSubmitting) => {
    await dispatch(addProductToCart(fields)).unwrap();
    setSubmitting(false);
  }

  const delProduct = (id) => dispatch(deleteProductFromCart(id))

  return {
    cart,
    status,
    addProduct,
    delProduct,
    total: cart.reduce((acc, { product }) => product.price + acc, 0)
  }
}
