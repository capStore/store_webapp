import { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { fetchList, setSearch as setSearchState } from '../module/store/products';

export const useProducts = ({ id = false, fetch = false }) => {
  const { products, status, search } = useSelector(state => state.products);
  const dispatch = useDispatch();

  useEffect(() => {
    if(fetch) dispatch(fetchList())
  }, [dispatch, fetch])

  const findBrandModelById = (id) => {
    const { brand, model } = products.find(product => product.id === id)
    return `${brand} ${model}`
  }

  const setSearch = (search) => {
    dispatch(setSearchState(search))
  }

  const filterByRegex = () => {
    const regex = new RegExp(search.replace(/\s/g,''), "gi");
    return products.filter(item => (item.brand + item.model).match(regex));
  }


  return {
    products: search ? filterByRegex() : products,
    loading: status === 'fetching',
    product: id && products.find(product => product.id === id),
    search,
    findBrandModelById,
    setSearch
  }
}
