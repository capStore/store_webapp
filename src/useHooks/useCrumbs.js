import { useLocation } from 'react-router-dom';
export const useCrumbs = () => {
  const location = useLocation();

  const generateCrumbs = (arrayLocation) => {
    let url = "";
    return arrayLocation.map(text => {
      url += `/${text}`
      return { text, url }
    });
  }
  const crumbs = generateCrumbs((location.pathname.split('/').filter(crumb => crumb)))

  return {
    crumbs
  }
}
