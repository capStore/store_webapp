import { Col, Card } from 'react-bootstrap'
import { LinkStyle as Link } from './styles'

export const CardItem = ({ id, model, brand, price, image }) => {
  return (
    <Col lg={3} md={6} className="my-2">
      <Link to={`/productos/${id}`}>
        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src={image} />
          <Card.Body>
            <Card.Title>{brand} {model} </Card.Title>
            <Card.Text>
              Precio: { price } €
            </Card.Text>
          </Card.Body>
        </Card>
      </Link>
    </Col>
  )
}
