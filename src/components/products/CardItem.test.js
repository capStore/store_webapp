import { render, cleanup } from '@testing-library/react';
import { CardItem } from './CardItem';
import { Link, MemoryRouter } from 'react-router-dom';
import '@testing-library/jest-dom/extend-expect';
afterEach(cleanup);

describe('CardItem', () => {
  it('should render the correct properties', () => {
    const { getByText } = render(
      <CardItem
        id="1"
        model="Model 1"
        brand="Brand 1"
        price={100}
      />,
      { wrapper: MemoryRouter }
    );

    const brand = getByText(/Brand 1/i);
    const model = getByText(/Model 1/i);

    expect(brand).toBeInTheDocument();
    expect(model).toBeInTheDocument();
  });
  it('should render router link', async () => {
    const { getByTestId } = render(
      <MemoryRouter>
        <Link to="/productos/1" data-testid="link">Product 1</Link>
      </MemoryRouter>
    );
    const link = getByTestId('link');
    expect(link).toHaveAttribute('href', '/productos/1');
  });
});