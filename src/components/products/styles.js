import styled from "styled-components";
import { Link } from 'react-router-dom'

export const ProductList = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill,minmax(15rem,1fr));
  grid-gap: 2rem;
`

export const LinkStyle = styled(Link)`
  text-decoration: none;
  color: #242526;
  &:hover {
    color: black;
    background-color: black
  }
`