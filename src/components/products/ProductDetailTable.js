import { Table } from 'react-bootstrap'
export const ProductDetailTable = ({ cpu, ram, display, dimensions, camera, battery, weight, so }) => {
  return (
    <Table striped bordered hover className='my-5'>
      <tbody>
        <tr>
          <td>Procesador:</td>
          <td>{cpu}</td>
          <td>Memoria Ram:</td>
          <td>{ram} GB</td>
        </tr>
        <tr>
          <td>Dimensiones:</td>
          <td>{dimensions}</td>
          <td>Pantalla:</td>
          <td>{display}</td>
        </tr>
        <tr>
          <td>Sistema:</td>
          <td>{so}</td>
          <td>Camara:</td>
          <td>{camera ? 'Si' : 'No'}</td>
        </tr>
        <tr>
          <td>Bateria:</td>
          <td>{battery}</td>
          <td>Peso:</td>
          <td>{weight}</td>
        </tr>

      </tbody>
    </Table>
  )
}
