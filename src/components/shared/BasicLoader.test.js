import { BasicLoader } from './BasicLoader';
import { render } from '@testing-library/react';

describe('BasicLoader component', () => {
  it('renders a Spinner component with color white', () => {
    const { container } = render(<BasicLoader color='white'/>)
    expect(container.querySelector('[color="white"]')).toBeInTheDocument()
  })
});