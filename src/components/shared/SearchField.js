import { useRef } from 'react'
import { Button, Form, InputGroup } from 'react-bootstrap';
import { MdClear } from "react-icons/md";
import { useProducts } from '../../useHooks/useProducts';

export const SearchField = () => {
  const { search, setSearch } = useProducts({});
  const searchRef = useRef(null);

  return (
    <InputGroup className="mb-3">
      <Form.Control
        placeholder="Buscar producto"
        value={ search }
        ref={searchRef}
        onChange={({ target }) => setSearch(target.value)}
      />
      <Button className='text-dark border-left-0 border bg-light' onClick={() => setSearch("")}>
        <MdClear />
      </Button>
    </InputGroup>
  )
}
