import { Spinner } from "react-bootstrap"

export const BasicLoader = ({ color = 'white' }) => {
  return <Spinner animation="border" size="sm" color={color} role="status"></Spinner>
}
