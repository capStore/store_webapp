import styled from "styled-components";
import { Badge, Breadcrumb, Navbar } from "react-bootstrap";

export const CustomBadge = styled(Badge)`
  position: absolute;
  font-size: 10px;
  top: 10px;
  margin-left: 2px;
`

export const NavbarStyle = styled(Navbar)`
  color: white;
  a {
    color: white
  }
`

export const BreadcrumbCustom = styled(Breadcrumb)`
  ol {
    margin: 0;
    text-transform: capitalize;
  }
`