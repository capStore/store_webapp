import { Container, Breadcrumb, Navbar } from 'react-bootstrap';
import { BsFillCartFill } from 'react-icons/bs'
import { Link } from 'react-router-dom';
import { isId } from '../../module/api/helpers';
import { useCart } from '../../useHooks/useCart';
import { useCrumbs } from '../../useHooks/useCrumbs'
import { useProducts } from '../../useHooks/useProducts';
import { CustomBadge, NavbarStyle, BreadcrumbCustom } from './styles';

export const Header = () => {
  const { crumbs } = useCrumbs();
  const { findBrandModelById } = useProducts({})
  const { cart } = useCart()
  return (
    <NavbarStyle bg="dark" variant="dark">
      <Container>
        <Navbar.Brand as={Link} to="/">
          Computadores
        </Navbar.Brand>
        <BreadcrumbCustom>
          <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/' }}> Tienda </Breadcrumb.Item>
          {
            crumbs && crumbs.map((crumb, key) => 
              <Breadcrumb.Item key={key} linkAs={Link} linkProps={{ to: crumb.url }} active={(key + 1) === crumbs.length}>{ isId(crumb.text) ? findBrandModelById(crumb.text) : crumb.text }</Breadcrumb.Item>
            )
          }
          
        </BreadcrumbCustom>
        <Link to="/carrito">
          <BsFillCartFill size={25} /> 
          <CustomBadge bg="primary" pill={true}>{ cart.length }</CustomBadge>
        </Link>
      </Container>
    </NavbarStyle>
  )
}
