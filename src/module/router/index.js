import { createBrowserRouter, Navigate } from 'react-router-dom';
import { MainPage } from '../../pages/MainPage';
import { ProductDetail } from '../../pages/ProductDetail';
import { Products } from '../../pages/Products';
import { CartPage } from '../../pages/CartPage';

export const router = createBrowserRouter([
  {
    path: '/productos',
    element: <MainPage />,
    children: [
      { path: '', element: <Products />, },
      { path: ':id', element: <ProductDetail /> },
    ]
  },
  {
    path: '/carrito',
    element: <MainPage />,
    children: [
      { path: '', element: <CartPage />, }
    ]
  },
  {
    path: '/',
    element: <Navigate to="productos" />
  },
  {
    path: '*',
    element: <h1>Not found</h1>,
  },
]);