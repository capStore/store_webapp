import { render } from '@testing-library/react';
import { Navigate, RouterProvider, createMemoryRouter } from 'react-router-dom';
import { MainPage } from '../../pages/MainPage';
import { Products } from '../../pages/Products';

export const routes = [
  {
    path: '/productos',
    element: <MainPage />,
    children: [
      { path: '', element: <Products />, },
    ]
  },
  { path: '/productos/:id', element: <div>aaaaaaa</div> },
  {
    path: '/',
    element: <Navigate to="products" />
  },
  {
    path: '*',
    element: <h1>Not found</h1>,
  },
]

const routerMainPage = createMemoryRouter(routes, {initialEntries: ["/productos"]});
const routerProductDetail = createMemoryRouter(routes, {initialEntries: ["/productos/test"]});
const routerNotFound = createMemoryRouter(routes, {initialEntries: ["/unknown"]});
jest.mock('../../pages/MainPage', () => ({ MainPage: jest.fn() }));
jest.mock('../../pages/Products', () => ({ Products: jest.fn() }));

describe('router', () => {
  it('renders the main page as the default route', () => {
    MainPage.mockImplementation(() => <div>main page</div>)
    const { getByText } = render(<RouterProvider router={ routerMainPage } />);
    const mainPageElement = getByText(/main page/i);
    expect(mainPageElement).toBeInTheDocument();
  });

it('renders the products page when visiting /productos', () => {
    Products.mockImplementation(() => <div data-testid="Products">Products</div>)
    const { getByText } = render(<RouterProvider router={ routerProductDetail } />);
    const productsPageElement = getByText(/aaaaaaa/i);
    expect(productsPageElement).toBeInTheDocument();
  });

   it('renders the detail page when visiting /productos/1', () => {
    const { getByText } = render(<RouterProvider router={ routerNotFound } />);
    const detailPageElement = getByText(/Not found/i);
    expect(detailPageElement).toBeInTheDocument();
  });
});