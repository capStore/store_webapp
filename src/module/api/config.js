import axios from 'axios';
const { REACT_APP_API_URL } = process.env;

export const RestAPI = axios.create({
  baseURL: REACT_APP_API_URL || 'https://capstore-service.onrender.com/'
});
