import { RestAPI } from './config'

export const get = async (url, params) => {
  try {
    return (await RestAPI.get(url, { params })).data;
  } catch (error) {
    throw new Error(`${error.message}`)
  }
}

export const post = async (url, data) => {
  try {
    return (await RestAPI.post(url, data)).data;
  } catch (error) {
    throw new Error(`${error.message}`)
  }
}

export const del = async (url) => {
  try {
    return (await RestAPI.delete(url)).data;
  } catch (error) {
    throw new Error(`${error.message}`)
  }
}

export const compareDates = (date, date2 = new Date()) =>  {
  const differenceInMilliseconds = Math.abs(new Date(date).getTime() - date2.getTime());
  const differenceInHours = differenceInMilliseconds / 1000 / 60 / 60;
  return differenceInHours > 1;
}

export const isId = (text) => {
  const idFormat = /^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/;
  return idFormat.test(text);
}