import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { compareDates, get } from '../api/helpers';

const initialState = {
  updatedAt: undefined,
  search: "",
  status: "nofeched",
  products: []
};

export const productSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    setSearch: (state, { payload: search }) => ({...state, search })
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchList.pending, (state) => ({ ...state, status: 'fetching' }))
      .addCase(fetchList.fulfilled, (state, action) => {
        const { error, data, persist } = action.payload
        if (!error) return state = { ...state, products: data, status: "fetched", updatedAt: persist ? state.updatedAt : new Date().toISOString() }
      })
  },
});

export const fetchList = createAsyncThunk(
  'products/list',
  async (_, { getState }) => {
    const { products } = getState()
    if (products.products && products.products.length && products.updatedAt && !compareDates(new Date(products.updatedAt))) return { error: false, data: products.products, persist: true };
    return get(`/product`);
  }
);

export const { setSearch } = productSlice.actions;

export default productSlice.reducer;