import { configureStore } from '@reduxjs/toolkit';
import { get } from '../api/helpers';
import productsReducer, { productSlice, fetchList } from './products';

jest.mock('../api/helpers', () => ({
  get: jest.fn()
}));

const product = {
  id: "dfd91885-8c70-49bd-ad39-96f0c4955665",
  price: 636.25,
  brand: "Asus",
  model: "TUF Gaming F15",
  ram: 16,
  cpu: "Intel® Intel® Core™ i5-10300H (Caché = 8MB SmartCache, 2.5GHz hasta 4.5GHz, 64-bit)",
  display: "15.6\"",
  camera: true,
  battery: "3 celdas Ion de litio 48W/h",
  so: "Windows 11",
  dimensions: "59 x 256 x 24,7~24,9 mm",
  weight: 2.3
}

const mockGet = () => get.mockImplementation(() => {
  return Promise.resolve({ error: false, data: [ product ] })
});

describe('productSlice', () => {
  let store;
  const initialState = {
    updatedAt: undefined,
    search: "",
    status: "nofeched",
    products: []
  };

  beforeEach(() => {
    store = configureStore({
      reducer: productSlice.reducer,
    });
  });

  it('should handle initial state', () => {
    expect(productsReducer(undefined, { type: 'unknown' })).toEqual(initialState);
  });

  it('should handle fetchList products', async () => {
    mockGet()
    await store.dispatch(fetchList());
    expect(store.getState().products).toEqual([product])
  });
});
