import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { del, post } from '../api/helpers';

const initialState = {
  updatedAt: undefined,
  status: "nofeched",
  cart: []
};

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(addProductToCart.fulfilled, (state, action) => {
        const { data } = action.payload;
        state.cart = data;
      })
  },
});

export const addProductToCart = createAsyncThunk('cart/addProduct', (product) => post(`/cart`, product));

export const deleteProductFromCart = createAsyncThunk('cart/addProduct', (id) => del(`/cart/${id}`));

export default cartSlice.reducer;