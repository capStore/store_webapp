import { configureStore } from '@reduxjs/toolkit';
import cartReducer, { cartSlice, addProductToCart, deleteProductFromCart } from '../store/cart';
import { post, del } from '../api/helpers';

jest.mock('../api/helpers', () => ({
  post: jest.fn(),
  del: jest.fn()
}));

const product = {
  colorCode: "Negro",
  storageCode: 64,
  id: "dfd91885-8c70-49bd-ad39-96f0c4955665",
  product: {
    id: "dfd91885-8c70-49bd-ad39-96f0c4955665",
    price: 636.25,
    brand: "Asus",
    model: "TUF Gaming F15",
    ram: 16,
    cpu: "Intel® Intel® Core™ i5-10300H (Caché = 8MB SmartCache, 2.5GHz hasta 4.5GHz, 64-bit)",
    display: "15.6\"",
    camera: true,
    battery: "3 celdas Ion de litio 48W/h",
    so: "Windows 11",
    dimensions: "59 x 256 x 24,7~24,9 mm",
    weight: 2.3
  }
}

const mockPost = () => post.mockImplementation((_, data) => Promise.resolve({error: false, data: [data]}));
const mockDelete = () => del.mockImplementation(() => Promise.resolve({error: false, data: []}));

describe('cartSlice', () => {
  let store;
  const initialState = {
    updatedAt: undefined,
    status: "nofeched",
    cart: []
  };

  beforeEach(() => {
    store = configureStore({
      reducer: cartSlice.reducer,
    });
  });

  it('should handle initial state', () => {
    expect(cartReducer(undefined, { type: 'unknown' })).toEqual(initialState);
  });

  it('should handle addProductToCart and deleteProductFromCart', async () => {
    mockPost()
    await store.dispatch(addProductToCart(product));
    expect(store.getState().cart).toEqual([ product ])

    mockDelete()
    await store.dispatch(deleteProductFromCart(product.product.id));
    expect(store.getState().cart).toEqual([])
  });
});
