import { configureStore } from '@reduxjs/toolkit';
import productReducer from './products';
import cartReducer from './cart';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
export const store = configureStore({
  reducer: {
    products: persistReducer({ key: 'products', storage }, productReducer),
    cart: persistReducer({ key: 'cart', storage }, cartReducer),
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: false
  }),
});

export const persistor = persistStore(store)