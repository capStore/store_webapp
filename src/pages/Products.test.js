import { render, waitFor } from '@testing-library/react';
import { Products } from './Products';
import { useProducts } from '../useHooks/useProducts';
import { MemoryRouter } from 'react-router-dom';
jest.mock('../useHooks/useProducts', () => ({ useProducts: jest.fn() }));

describe('Products', () => {
  it('renders products', async () => {
    useProducts.mockImplementation(() => ({
      loading: false,
      products: [
        { id: '1', model: 'Model 1', brand: 'Brand 1', price: 100 },
        { id: '2', model: 'Model 2', brand: 'Brand 2', price: 200 },
        { id: '3', model: 'Model 3', brand: 'Brand 3', price: 300 },
      ],
    }));
    const { getByText } = render(<Products />, {wrapper: MemoryRouter});
    const productsTitle = await waitFor(() => getByText('Lista de productos'));
    expect(productsTitle).toBeInTheDocument();

    const product1 = getByText(/Brand 1 Model 1/i);
    const product2 = getByText(/Brand 2 Model 2/i);
    const product3 = getByText(/Brand 3 Model 3/i);

    expect(product1).toBeInTheDocument();
    expect(product2).toBeInTheDocument();
    expect(product3).toBeInTheDocument();
  });
});