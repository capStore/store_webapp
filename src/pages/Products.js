import { CardItem } from "../components/products/CardItem";
import { useProducts } from "../useHooks/useProducts";
import { Col, Row } from 'react-bootstrap'
import { SearchField } from '../components/shared/SearchField'

export const Products = () => {
  const { products, loading } = useProducts({ fetch: true });

  if (loading) return <h1>Cargando</h1>
  return (
    <div>
      <div className='container mt-3'>
        <Row>
          <Col>
            <h2 className='mb-3'>Lista de productos</h2>
          </Col>
          <Col lg={3}>
            <SearchField />
          </Col>
        </Row>
        <Row>
          {
            !loading && products.length && products.map((products, key) => <CardItem key={key} {...products} />)
          }
        </Row>
      </div>
    </div>
  );
}
