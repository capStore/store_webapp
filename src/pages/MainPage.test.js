import { render, screen } from '@testing-library/react';
import { MainPage } from './MainPage';
import { Header } from '../components/shared';
import { MemoryRouter, Outlet, Route, Routes } from 'react-router-dom';

jest.mock('../components/shared', () => ({ Header: jest.fn() }));

const FakeComponent = () => <div>My fake component</div>;

describe('MainPage', () => {
  it('renders header and outlet', () => {
    Header.mockImplementation(() => <div data-testid="header">Header</div>)
    const { getByText } = render(<MainPage />);
    const header = getByText('Header');
    expect(header).toBeInTheDocument();
    const headerById = screen.getByTestId('header');
    expect(headerById).toBeInTheDocument();
  });
  it('should render as expected', () => {
    render(
      <MemoryRouter>
      <Routes>
        <Route path="/"element={<Outlet />}>
          <Route index element={<FakeComponent />} />
        </Route>
      </Routes>
    </MemoryRouter>
    );
    const component = screen.getByText('My fake component');
    expect(component).toBeInTheDocument();
  });
});