import { render, cleanup, fireEvent } from '@testing-library/react';
import { CartPage } from './CartPage';
import { useCart } from '../useHooks/useCart';

jest.mock('../useHooks/useCart', () => ({ useCart: jest.fn() }));

const cart = [
  { product: { id: '1', brand: 'Samsung', model: 'S20', price: 800 }, storageCode: 128, colorCode: 'Black' },
  { product: { id: '2', brand: 'Apple', model: 'iPhone X', price: 1000 }, storageCode: 256, colorCode: 'White' }
];
const delProduct = (id) => cart.filter(item => item.product.id !== id);
const total = cart.reduce((acc, { product }) => product.price + acc, 0)

const mockCart = () => useCart.mockImplementation(() => ({
  cart,
  delProduct,
  total 
}));

describe('CartPage', () => {
  afterEach(cleanup);

  it('should display the cart products', () => {
    mockCart()
    const { getByText } = render(<CartPage />);
    expect(getByText('Samsung')).toBeInTheDocument();
    expect(getByText('S20')).toBeInTheDocument();
    expect(getByText('128 GB')).toBeInTheDocument();
    expect(getByText('Black')).toBeInTheDocument();
    expect(getByText('800 €')).toBeInTheDocument();
    expect(getByText('Apple')).toBeInTheDocument();
    expect(getByText('iPhone X')).toBeInTheDocument();
    expect(getByText('256 GB')).toBeInTheDocument();
    expect(getByText('White')).toBeInTheDocument();
    expect(getByText('1000 €')).toBeInTheDocument();
    expect(getByText('Total')).toBeInTheDocument();
    expect(getByText('1000 €')).toBeInTheDocument();
  });

  it('should delete product from the cart', () => {
    mockCart();
    const { getByTestId } = render(<CartPage />);
    const trashButton = getByTestId('trash-button-1');
    fireEvent.click(trashButton);
    expect(useCart().delProduct("1")).toEqual([useCart().cart[1]]);
  });

  it('should display message if the cart is empty', () => {
    useCart.mockImplementation(() => ({
      cart: [],
    }));
    const { getByText } = render(<CartPage />);
    expect(getByText('No tienes productos agregados al carrito aún.')).toBeInTheDocument();
  });

  it('should display total in the cart', () => {
    mockCart();
    const { getByText } = render(<CartPage />);
    const getTotal = useCart().cart.reduce((acc, { product }) => product.price + acc, 0)
    expect(getByText(`${getTotal} €`)).toBeInTheDocument();
  });
});