import { Outlet } from 'react-router-dom'
import { Header } from '../components/shared'

export const MainPage = () => {
  return (
    <>
      <Header />
      <Outlet />
    </>
  )
}
