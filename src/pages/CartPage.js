import { Container, Table } from 'react-bootstrap'
import { useCart } from '../useHooks/useCart'
import { FaTrash } from "react-icons/fa";

export const CartPage = () => {
  const { cart, total, delProduct } = useCart();
  return (
    <Container>
      <h2 className='my-3'>Carrito</h2>
      <Table>
        <thead>
          <tr>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Almacenamiento</th>
            <th>Color</th>
            <th>Precio</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {
            cart.length ? cart.map(({ product, storageCode, colorCode }, key) => (
              <tr key={key}>
                <td>{product.brand}</td>
                <td>{product.model}</td>
                <td>{storageCode} GB</td>
                <td>{colorCode}</td>
                <td>{product.price} €</td>
                <td><button className='border-0 bg-light' data-testid={`trash-button-${product.id}`} onClick={() => delProduct(product.id)}><FaTrash size={18} /></button></td>
              </tr>
            )) : (
              <tr>
                <td colSpan={6} className='text-center py-4 fw-bold'>No tienes productos agregados al carrito aún.</td>
              </tr>
            )
          }
          <tr>
            <td colSpan={4} ></td>
            <td className='fw-bold' >Total</td>
            <td className='text-right fw-bold'> { total } €</td>
          </tr>
        </tbody>
      </Table>
    </Container>
  )
}
