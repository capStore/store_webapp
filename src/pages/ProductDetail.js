import { Button, Row, Col, Form, Container } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { useProducts } from '../useHooks/useProducts';
import { Formik, Form as FormikForm } from 'formik';
import { BasicLoader } from '../components/shared/BasicLoader';
import { useCart } from '../useHooks/useCart';
import { ProductDetailTable } from '../components/products/ProductDetailTable';

export const ProductDetail = () => {
  const { id } = useParams();
  const { product } = useProducts({ id });
  const { addProduct } = useCart();
  return (
    <Container className='mt-5'>
      <Row>
        <Col lg={6} className='text-center'>
          <img src={product.image} style={{ width: '80%' }} alt={`${ product.brand } ${ product.model }`}/>
        </Col>
        <Col lg={6}>
          <div className='d-flex justify-content-between'>
            <h2>{product.brand} {product.model}</h2>
            <h2>{product.price} €</h2>
          </div>
          <ProductDetailTable {...product} />
          
          <Formik
            initialValues={{ colorCode: 'Negro', storageCode: 64 }}
            onSubmit={(fields, actions) => addProduct({ ...fields, id }, actions.setSubmitting)}
          >
            {
              ({ isSubmitting, handleChange }) => {
                return (
                  <FormikForm>
                    <Row>
                      <Col className="pe-lg-5" lg={6}>
                        <Form.Label>Color</Form.Label>
                        <Form.Select aria-label="Seleccionar color" name="colorCode" onChange={handleChange}>
                          <option value="Negro">Negro</option>
                          <option value="Blanco">Blanco</option>
                        </Form.Select>
                      </Col>
                      <Col className="ps-lg-5" lg={6}>
                        <Form.Label>Almacenamiento</Form.Label>
                        <Form.Select aria-label="Seleccionar color" name="storageCode" onChange={handleChange}>
                          <option value="64">64 GB</option>
                          <option value="128">128 GB</option>
                          <option value="400">480 GB</option>
                          <option value="500">500 GB</option>
                          <option value="1000">1 TB</option>
                        </Form.Select>
                      </Col>
                    </Row>
                    <div className="text-end mt-4">
                      <Button className="btn prim" disabled={isSubmitting} type="submit">
                        {
                          isSubmitting ? <BasicLoader /> : "Agregar al carrito"
                        }
                      </Button>
                    </div>
                  </FormikForm>
                )
              }
            }
          </Formik>
        </Col>
      </Row>
    </Container>
  )
}
