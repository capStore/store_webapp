import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { store, persistor } from './module/store';
import './index.css';
import { RouterProvider } from 'react-router-dom';
import { router } from './module/router';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
createRoot(document.getElementById('root')).render(
  <Provider store={store} persistor={persistor}>
    <RouterProvider router={router} />
  </Provider>
)
reportWebVitals();