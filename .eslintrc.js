module.exports = {
	"env": {
		"browser": true,
		"node": true,
		"es2021": true,
		"jest": true
	},
	"extends": [
		"eslint:recommended",
		"plugin:react/recommended",
		'plugin:react/jsx-runtime'
	],
	"overrides": [
	],

	"parserOptions": {
		"ecmaVersion": 2018,
		"sourceType": "module",
		"ecmaFeatures": {
			"jsx": true,
			"experimentalObjectRestSpread": false
		},
	},
	"plugins": [
		"react"
	],
	"rules": {
		"react/prop-types": 0,
		"prefer-destructuring": [
			"error",
			{
				"array": true,
				"object": true
			},
			{
				"enforceForRenamedProperties": false
			}
		]
	}
}
